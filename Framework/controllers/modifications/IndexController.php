<?php
/**
 * BXFork
 *
 * Framework basico para el desarrollo web.
 *
 * @author 		Jovani Pérez (@BlackBlex)
 * @license 	General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package 	controllers
 * @subpackage  Modifications
 *
 * ==============Information==============
 *      Filename: IndexController.php
 *          Path: ./controllers/modifications/
 * ---------------------------------------
*/

class IndexController extends Controller
{

	//Toda modificación se hara apartir de aquí

	//Before

	//After

	public function logout()
	{
		session_destroy();
		$this->redirect();
	}

	public function login($message = false)
	{
		global $Help;
		if ( isset($_SESSION["u_user"]) )
			$this->redirect();

		if ( $message )
		{
			$this->message = $message;
		}

		if ( isset($_POST["user"]) )
		{
			$user = $Help->model("Users");
			$user->setUser($_POST["user"]);
			$password = $_POST["pass"];
			if ( $user->check() )
			{
				if ( $user->verify($password) )
				{
					$_SESSION["u_id"] = $user->getId();
					$_SESSION["u_user"] = $user->getUser();
					$_SESSION["u_pass"] = $password;
					$_SESSION["u_avatar"] = $user->getAvatar();

					$this->redirect();
				}
				else
				{
					$message = "nopassword";
					$this->redirect("user", "login/" . $message);
					// Error password not equals.
				}
			}
			else
			{
				$message = "nouser";
				$this->redirect("user", "login/" . $message);
				// Error user not found.
			}

		}
	}

	public function create()
	{
		global $Help;
		if ( isset($_POST["user"]) )
		{
			$user = $Help->model("Users");
			$user->setUser($_POST["user"]);
			if ( !$user->check() )
			{
				$user->setPassword($_POST["pass"]);
				$update = $user->insert();
			}
			else
			{
				$message = "euser";
				$this->redirect("user", "register/" . $message);
			}

		}
		$this->redirect();
	}

	public function register($message = false)
	{
		if ( $message )
		{
			$this->message = $message;
		}
	}

}
?>