<?php
/**
 * BXFork
 *
 * Framework basico para el desarrollo web.
 *
 * @author 		Jovani Pérez (@BlackBlex)
 * @license 	General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package 	controllers
 *
 * ==============Information==============
 *      Filename: InstallController.php
 *          Path: ./controllers/
 * ---------------------------------------
*/

class InstallController extends Install
{

	public function index($modInstall)
	{
		global $allFunctions;
		
		$this->mod = $modInstall;
		
		if ( isset($modInstall) && !empty($modInstall) )
		{
			$modInstallI = ucfirst($modInstall) . DS .'info';
			$modInstall = ucfirst($modInstall) . DS .'install.in';
			if ( is_readable(PACKAGE_PATH . $modInstall) && is_file(PACKAGE_PATH . $modInstall) )
			{
				require_once(PACKAGE_PATH . $modInstall);
				require_once(PACKAGE_PATH . $modInstallI);
			}
		}

		   $this->name = $name;
		   $this->desc = $description;
		 $this->author = $author;
		$this->version = $version;

		$this->actions = $this->installArray;

	}

	public function success($modInstall)
	{
		global $allFunctions;

		$this->_install = true;
		
		if ( isset($modInstall) && !empty($modInstall) )
		{
			$modInstall = ucfirst($modInstall) . DS .'install.in';
			if ( is_readable(PACKAGE_PATH . $modInstall) && is_file(PACKAGE_PATH . $modInstall) )
			{
				require_once(PACKAGE_PATH . $modInstall);
			}
		}

		$this->changes = $this->installArraySuccess;
		
		//$this->redirect('install', 'ready');
	}

}
?>