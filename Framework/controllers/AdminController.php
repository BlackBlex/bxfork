<?php
/**
 * BXFork
 *
 * Framework basico para el desarrollo web.
 *
 * @author 		Jovani Pérez (@BlackBlex)
 * @license 	General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package 	controllers
 *
 * ==============Information==============
 *      Filename: AdminController.php
 *          Path: ./controllers/
 * ---------------------------------------
*/

class AdminController extends AdmController
{

	public function index()
	{
		global $Help;
		$this->section = 'dashboard';

		$Help->loadModel('AdminModels');
		$sqlAdmin = $Help->model('AdminModels');

		$this->countUsers = $sqlAdmin->getRegisteredUsers();
		$this->lastUser = $sqlAdmin->getLastUser();
		$this->version = VERSION_APP;
	}

	public function config($message = false)
	{
		global $_Variables, $Help;
		$this->section = 'config';

		if ( $message )
		{
			$this->message = $message;
		}

		$this->config_site = $_Variables->getVar('config')['site'];
		$this->config_options = $_Variables->getVar('config')['options'];
		$this->config_database = $_Variables->getVar('config')['database'];

		$this->config_site['url'] = str_replace('http://', '', $this->config_site['url']);
		$this->config_site['url'] = str_replace('/' . $this->config_site['directory'] , '', $this->config_site['url']);

		foreach (glob(LANG_PATH . '*') as $dir) 
		{
			if ( is_dir($dir) )
				$this->language[] = str_replace(LANG_PATH, '', $dir);
		}

		$this->defaultTheme = $_Variables->getVar('themes');

		if ( isset($_POST['save']) )
		{
			foreach ($_POST as $key => $value) {
				if ( strpos($key, 'inputsite') !== false)
					$this->config['site'][str_replace('inputsite', '', $key)] = $value;
				elseif ( strpos($key, 'inputoptions') !== false )
					$this->config['options'][str_replace('inputoptions', '', $key)] = $value;
				elseif ( strpos($key, 'inputdatabase') !== false )
					$this->config['database'][str_replace('inputdatabase', '', $key)] = $value;
			}

			if ( $this->config['options']['multitheme'] == 'on' )
				$this->config['options']['multitheme'] = true;
			else
				$this->config['options']['multitheme'] = false;

			if ( $this->config['options']['displayErrors'] == 'on' )
				$this->config['options']['displayErrors'] = true;
			else
				$this->config['options']['displayErrors'] = false;

			$message = 'f' . $Help->saveFile(CORE_PATH . 'config' . DS . 'config.json', json_encode($this->config, JSON_PRETTY_PRINT));
			$this->redirect('admin', 'config/' . $message);
		}

	}

	public function pages($page = false, $message = false, $result = false)
	{
		global $_View;
		$this->section = 'pages';

		if ( $result )
		{
			$this->result = $result;
		}

		if ( !$page || $page == 'none')
		{
			foreach (glob(CONTROLLER_PATH . '*') as $file) 
			{
				if ( is_file($file) && strpos($file, 'AdminController') === false)
				{
					$class = str_replace('.php', '', str_replace(CONTROLLER_PATH, '', $file));
					require_once($file);
					$f = new ReflectionClass($class);
					$farray = $f->getMethods();
					foreach ($farray as $key => $value) {
						if ( $value->class == $class)
							$this->functions[$class][] = $value->name;
					}
					unset($f);
				}
			}
		}
		elseif ( $page == 'add')
		{
			$_View->setView('pagesA');
			$this->controller = $message;

			if ( isset($_POST['save']) )
			{
				$data = file_get_contents(CONTROLLER_PATH . $_POST['controller'] . '.php');
				$data = str_replace('}
?>', '	public function ' . $_POST['namepage'] . '(' . $_POST['parmpage'] . ')
	{

	}

}
?>', $data);

				if (file_put_contents(CONTROLLER_PATH . $_POST['controller'] . '.php', $data) )
				{
					if ( copy(RESOURCE_PATH . 'blankView.tpl', THEME_VIEW_PATH . strtolower(str_replace('Controller', '', $_POST['controller'])) . DS . $_POST['namepage'] . 'View.tpl') )
						$result = 'f1';
					else
						$result = 'f0';

					$this->redirect('admin', 'pages/none/none/' . $result);
				}

			}
		}
		else
			$this->redirect('admin', 'pages');

	}

	public function menus($page = false, $message = false, $result = false)
	{
		global $_View;
		$this->section = 'menus';

		if ( $result )
		{
			$this->result = $result;
		}

		if ( !$page || $page == 'none')
		{
			if ( is_file(THEME_PATH . 'config.json') )
			{	
				$config = file_get_contents(THEME_PATH . 'config.json');
				$config = json_decode($config, true);
				$menu = $config['menu'];

				foreach ($menu as $key => $value) 
				{	
					foreach ($value as $keyi => $valuei) {
						$this->items[$key][] = [$keyi, $valuei['title'], $valuei['href'], $valuei['show']];
					}
				}
			}
		}
		elseif ( $page == 'add')
		{
			$_View->setView('menusA');
			$this->menuindex = $message;

			if ( isset($_POST['save']) )
			{
				if ( is_file(THEME_PATH . 'config.json') )
				{	
					$config = file_get_contents(THEME_PATH . 'config.json');
					$config = json_decode($config, true);

					$config['menu'][$_POST['menuindex']][$_POST['nameitem']] = [
						'title' => $_POST['titleitem'],
						'href'  => $_POST['hrefitem'],
						'show'  => $_POST['showitem']
					];

					if ( file_put_contents(THEME_PATH . 'config.json', str_replace('\\', '',json_encode($config, JSON_PRETTY_PRINT))) )
						$result = 'f1';
					else
						$result = 'f0';

					$this->redirect('admin', 'menus/none/none/' . $result);
				}
			}
		}
		elseif ( $page == 'addm')
		{
			$_View->setView('menusAM');

			if ( isset($_POST['save']) && !empty($_POST['namesection']) && !empty($_POST['nameitem']) && !empty($_POST['showitem']) )
			{
				if ( is_file(THEME_PATH . 'config.json') )
				{	
					$config = file_get_contents(THEME_PATH . 'config.json');
					$config = json_decode($config, true);

					$config['menu'][$_POST['namesection']][$_POST['nameitem']] = [
						'title' => $_POST['titleitem'],
						'href'  => $_POST['hrefitem'],
						'show'  => $_POST['showitem']
					];

					if ( file_put_contents(THEME_PATH . 'config.json', str_replace('\\', '',json_encode($config, JSON_PRETTY_PRINT))) )
						$result = 'f1';
					else
						$result = 'f0';

					$this->redirect('admin', 'menus/none/none/' . $result);
				}
			}
		}
		else
			$this->redirect('admin', 'menus');

	}

	public function languages()
	{
		global $_Variables;
		$this->section = 'languages';

		/*$lang = file_get_contents(LANG_PATH . strtolower($_Variables->getVar('config')['options']['language']) . DS . 'lang');
		$lang = html_entity_decode($lang);
		$lang = json_decode($lang, true);
*/
		//echo '<pre>';
		print_r($lang);

	}

	public function packages()
	{
		$this->section = 'packages';
	}

	public function themes()
	{
		$this->section = 'themes';
	}
	
	public function login()
	{
		if ( !isset($_SESSION['u_user']) )
			$this->redirect();
		else
		{
			if ( $_SESSION['u_id'] == 1)
			{
				$_SESSION['flag'] = 'admin';
				$this->redirect('admin', 'index');
			}
			else
				$this->redirect();
		}
	}

}
?>