<?php
/**
 * BXFork
 *
 * Framework basico para el desarrollo web.
 *
 * @author 		Jovani Pérez (@BlackBlex)
 * @license 	General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package 	Core
 *
 * ==============Information==============
 *      Filename: Startup.php
 *          Path: ./core/
 * ---------------------------------------
*/

class Startup
{
	private $_modded		= false;
	private $_url			= '/';
	private $_controller = '';
	private $_action     = '';
	private $_args       = [];

	public function load()
	{
		global $_Variables, $_View;

		$this->loadThemes();

		if ( isset($_GET['url']) )
			$this->_url = filter_input(INPUT_GET, 'url', FILTER_SANITIZE_URL);

		$this->_url = $this->checkRoutes();
		$this->parseUrl();

		$_View->setView($this->_action);

		return $this->finish();
	}

	public function loadThemes()
	{
		global $_Variables;

		foreach (glob(THEMES_PATH . '*') as $dir) 
		{
			if ( is_dir($dir) )
				$themes[] = str_replace(THEMES_PATH, '', $dir);
		}
		$_Variables->setVar('themes', $themes);
	}

	public function checkRoutes()
	{
		$routes = file_get_contents(CORE_PATH . 'config' . DS . 'routes.json');
		$routes = json_decode($routes, true);
		$routes = $routes['url'];

		#
		#	Comprobamos si hay alguna redirección exacta a alguna url,
		#	si la hay, checamos si es de alguna modificación
		#
		if (isset($routes[$this->_url]))
		{
			if ( false !== strpos($routes[$this->_url],'mod/') )
				$this->_modded = true;
        	return $routes[$this->_url];
		}

		#
		#	En caso de no encontrar una redirección exacta,
		#	comprobamos si pertenece a una redirección indirectamente,
		#
		foreach ($routes as $key => $val) 
		{
			if (strripos($key, '*', -1))
			{
				$key = rtrim($key, '*');
				if (strncmp($this->_url, $key, strlen($key)) == 0)
				{
					if ( false !== strpos($val,'mod/') )
						$this->_modded = true;
					return str_replace($key, rtrim($val, '*'), $this->_url);
				}
			}
		}

		#
		#	En caso de no encontrar ni una redirección,
		#	devolvemos tal cual la url.
		#
		return $this->_url;
	}

	public function parseUrl()
	{
		global $_Variables;
		$url = explode('/', $this->_url);
		$url = array_filter($url);
		if ( $this->_modded )
			$die = strtolower(array_shift($url));

		$this->_controller = strtolower(array_shift($url));

		$this->_action = strtolower(array_shift($url));
		$this->_args   = $url;
	
		if ( !$this->_controller )
			$this->_controller = $_Variables->getVar('config')['options']['defaultController'];

		if ( !$this->_action )
			$this->_action     = $_Variables->getVar('config')['options']['defaultAction'];

		if ( !isset($this->_args) )
			$this->_args       = [];

	}

	public function finish()
	{
		$controllerObj		= false;
		$controller 		= ucwords($this->_controller) . 'Controller';
		$fileController 	= CONTROLLER_PATH . $controller . '.php';

		if ( $this->_modded )
			$fileController  = MOD_PATH . $controller . '.php';

		if ( is_file($fileController) )
			require $fileController;
		else
			throw new Exception("Request processing failed", 11);

		return $this->loadController($controller);
	}

	public function loadController($controller)
	{
		$controllerObj = new $controller($this->_controller, $this->_action, $this->_args);

		if ($controllerObj->load(true) === false)
			return $controllerObj;

		try
		{
			$reflectionMethod = new ReflectionMethod($controllerObj, $controllerObj->c_action);
		}
		catch (ReflectionException $e)
		{
			throw new Exception("Request processing failed", 12);
		}

		$num_params = count($controllerObj->c_args);

		if ($controllerObj->c_limit && ($num_params < $reflectionMethod->getNumberOfRequiredParameters() ||
			$num_params > $reflectionMethod->getNumberOfParameters()))
		{
			throw new Exception("Request processing failed", 13);
		}

		try {
			$reflectionMethod->invokeArgs($controllerObj, $controllerObj->c_args);
		} catch (ReflectionException $e) {
			throw new Exception("Request processing failed", 14);
		}

		$controllerObj->load();

		return $controllerObj;
	}

}

?>