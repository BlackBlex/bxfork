<?php
/**
 * BXFork
 *
 * Framework basico para el desarrollo web.
 *
 * @author 		Jovani Pérez (@BlackBlex)
 * @license 	General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package 	Libs
 *
 * ==============Information==============
 *      Filename: Variables.php
 *          Path: ./core/libs/
 * ---------------------------------------
*/

class Variables
{
	/**
	 * Variable que contendra todas las demás variables.
	 * @var array
	 */
	private $_vars;

	/**
	 * Establecemos una nueva variable con un indice y valor.
	 * @param string $key Nombre del indice
	 * @param string|array $value Valor del indice o valores (array)
	 * @return null
	 */
	public function setVar($key, $value)
	{
		$this->_vars[$key] = $value;
	}

	/**
	 * Obtenemos una variable guardada.
	 * @param string $key Nombre del indice
	 * @return string|array
	 */
	public function getVar($key = false)
	{
		if ( !$key )
			return $this->_vars;
		else
		{
			if ( isset($this->_vars[$key]) )
				return $this->_vars[$key];
			else
				return null;
		}
	}

	/**
	 * Imprimimos todas las variables que hay guardadas.
	 * @return null
	 */
	public function printf($key = false)
	{
		echo '<pre>';
		print_r($this->getVar($key));
		echo '</pre>';
	}
}

?>