<?php
/**
 * BXFork
 *
 * Framework basico para el desarrollo web.
 *
 * @author 		Jovani Pérez (@BlackBlex)
 * @license 	General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package 	Obj
 * @subpackage  Install
 *
 * ==============Information==============
 *      Filename: Install.php
 *          Path: ./core/obj/install/
 * ---------------------------------------
*/

class Install extends Controller
{
	use DataBase;

	/**
	 * Contiene el contenido del archivo que se esté trabajando.
	 * @var string
	 * @access private
	 */
	private $_data;

	/**
	 * Contiene la ruta del archivo que se esté trabajando.
	 * @var string
	 * @access private
	 */
	private $_file;

	/**
	 * Es una condicionante para que las funciones de esta clase se ejecuten o no.
	 * 
	 * Se usa para realizar una lista de confirmación de todas las acciones
	 * antes de proceder a instalar el paquete.
	 * false | No se ejecuta la función
	 * true  | Ejecuta la función
	 * 
	 * @var boolean
	 * @access public
	 */
	public $_install = false;

	/**
	 * Contiene todas las acciones que se realizaran en la instalación del paquete.
	 * 
	 * Esta variable es empleada en la lista de confirmación.
	 * 
	 * @var array
	 * @access public
	 */
	public $installArray = [];

	/**
	 * Contiene el resultado de las acciones una vez realizadas.
	 * 
	 * Esta variable es empleada en la lista que se realiza despues de la instalación,
	 * para verificar que todo salio bien o no.
	 * false | Indica que "x" acción no se completo. 
	 * true  | Indica que "x" acción se completo.
	 * 
	 * @var array
	 * @access public
	 */
	public $installArraySuccess = [];

	public function __construct($controller, $action, $args)
	{
		global $allFunctions;
		$this->DBinit();
		$this->c_controller = $controller;
		$this->c_action = $action;
		$this->c_args = $args;
		$this->init();

		$allFunctions = array_merge($allFunctions, [
			'mod_file'   		=> 'loadFile',
			'mod_replace'		=> 'replace',
			'mod_after'  		=> 'addAfter',
			'mod_before' 		=> 'addBefore',
			'mod_exit'	 		=> 'write',
			'mod_del'	 		=> '099908',
			'mod_move_file'		=> 'moveFile',
			'mod_replace_e'		=> 'replaceE',
			'add_route'  		=> 'addRoute',
			'add_lang'			=> 'addLang',
			'add_theme_config'	=> 'addThemeConfig',

			'db_add_column'   	=> 'add_column',
			'db_create_table' 	=> 'create_table',
			'db_insert'      	=> 'insert',
		]);

	}

	/**
	 * Resetea el contenido de las variables $_data & $_file.
	 * @return null
	 */
	public function init()
	{
		$this->_data   = '';
		$this->_file   = '';
	}

	/**
	 * Carga el archivo que se utilizara para la modificación.
	 * 
	 * Obtiene el contenido del archivo y lo pone en $_data,
	 * mientras que la ruta del archivo la guarda en $_file.
	 * Si el archivo no existe, se llama al metodo createFile.
	 * NOTA: El archivo se cargara desde la carpeta de controladores modificados.
	 * 		 No se permite la carga de archivos externos a esa carpeta.
	 * 
	 * @param string $filename Nombre del archivo.
	 * @return null
	 */
	public function loadFile($filename)
	{
		if ( !$this->_install)
		{
			$this->installArray[] = ['mod_file', [$filename]];
			return false;
		}

		$this->init();

		$this->_file = MOD_PATH . $filename;

		if ( is_readable($this->_file) && is_file($this->_file) )
			$this->_data = file_get_contents($this->_file);
		else
		{
			$this->createFile($filename);
			$this->_data = file_get_contents($this->_file);
		}

		$this->installArraySuccess[] = ['mod_file', $filename, is_file($this->_file)];
	}

	/**
	 * Mueve el archivo de la carpeta del paquete.
	 * 
	 * Se emplea para cuando el paquete contiene archivos extras,
	 * tales como modelos, archivos de lenguaje propio (en caso de usar propio controlador),
	 * recursos como JS, CSS, TPL, IMG, HEADERS, FOOTERS, etc.
	 * El archivo origen debe estar en una carpeta llamada "files".
	 * La ruta de destino debe de existir.
	 * NOTA: Realmente lo que hace esta función es copiar el archivo, no moverlo. :)
	 * 
	 * @param string $file Nombre del archivo que va a mover.
	 * @param string $newPath Ruta de destino.
	 * @return type
	 */
	public function moveFile($file, $newPath)
	{
		$this->init();

		$path = PACKAGE_PATH . $this->c_args[0] . DS . 'files' . DS . $file;
		$success = true;

		if ( !$this->_install)
		{
			$this->installArray[] = ['mod_move_file', [$file, DS . str_replace(MAIN_PATH, '', $newPath)]];

			if ( !file_exists($path) )
				errorFunction(40, "Installer processing failed");

			return false;
		}

		if ( is_readable($path) && is_file($path) )
		{
			if ( !file_exists($newPath . DS . $file)	)
				if ( !copy($path, $newPath . DS . $file) )
					$success = false;
		}

		$this->installArraySuccess[] = ['mod_move_file', $file, $success];
	}

	/**
	 * Agrega una nueva ruta.
	 * @param string $route Ruta con la cual accionaras.
	 * @param string $routeTo Ruta a la cual redireccionaras.
	 * @return null
	 */
	public function addRoute($route, $routeTo)
	{
		if ( !$this->_install)
		{
			$this->installArray[] = ['add_route', [$route, $routeTo]];
			return false;
		}

		$this->init();

		$this->_file = CORE_PATH . 'config' . DS . 'routes.json';
		$success = true;

		if ( is_readable($this->_file) && is_file($this->_file) )
		{
			$this->_data = file_get_contents($this->_file);
			$routesTemp = json_decode($this->_data, true);

			$routesTemp['url'][$route] = $routeTo;

			$this->_data = str_replace('\\', '',json_encode($routesTemp, JSON_PRETTY_PRINT));
			$this->write(true);
		}
		else
			$success = false;

		$this->installArraySuccess[] = ['add_route', $route, $success];
	}

	/**
	 * Agrega una nueva entrada al archivo de lenguaje-
	 * @param string $lang Lenguaje al que le quieres agregar la nueva entrada [EN, ES, IT, BR, etc].
	 * @param string $controller Controlador al que pertenece.
	 * @param string $action Acción al que pertenece.
	 * @param string|array $value Valor o array de valores con el siguiente formato ( [ 'test' => 'Esto es un ejemplo' ]).
	 * @return null
	 */
	public function addLang($lang, $controller, $action, $value)
	{
		$this->init();

		if ( strtolower($controller) != 'global')
			$this->_file = LANG_PATH . strtolower($lang) . DS . $controller . DS . 'lang';
		else
			$this->_file = LANG_PATH . strtolower($lang) . DS . 'lang';

		$success = true;

		if ( strtolower($controller) != 'global')
			$value = array_map('htmlentities', $value);
		else
			$value = htmlentities($value);

		if ( !$this->_install)
		{
			$this->installArray[] = ['add_lang', [$lang, $controller, $action, json_encode($value, JSON_PRETTY_PRINT)]];

			if ( !file_exists($this->_file) )
				errorFunction(41, "Installer processing failed");

			return false;
		}

		if ( is_readable($this->_file) && is_file($this->_file) )
		{
			$this->_data = file_get_contents($this->_file);
			$langTemp = json_decode($this->_data, true);

			$langTemp[$controller][$action] = $value;

			$this->_data = json_encode($langTemp, JSON_PRETTY_PRINT);
			$this->write(true);
		}
		else
			$success = false;

		$this->installArraySuccess[] = ['add_lang', $controller . '; ' . $action , $success];
	}

	/**
	 * Agrega una nueva entrada a archivo de configuración del tema que este por defecto.
	 * @param string $op Si es para un modulo o menu. [module | menu].
	 * @param string $after El nombre de la entrada existente de la cual se posicionara la nueva entrada.
	 * @param string $item Nombre de la nueva entrada.
	 * @param string|array $value Valor o array de valores con el siguiente formato ( [ 'test' => 'Esto es un ejemplo' ]).
	 * @return null
	 */
	public function addThemeConfig($op, $after, $item, $value)
	{
		$this->init();

		$this->_file = THEME_PATH . 'config.json';
		$success = true;

		if ( !$this->_install)
		{
			$this->installArray[] = ['add_theme_config', [$op, $after, $item, str_replace('\\', '',json_encode($value, JSON_PRETTY_PRINT))]];

			if ( !file_exists($this->_file) )
				errorFunction(42, "Installer processing failed");

			return false;
		}

		if ( is_readable($this->_file) && is_file($this->_file) )
		{
			$this->_data = file_get_contents($this->_file);
			$opTemp = json_decode($this->_data, true);

			$control = false;
			$icontrol = 0;

			$ite[$item] = $value;

			$pos = array_search($after, array_keys($opTemp[$op]));
			$opTemp[$op] = array_merge(
				array_slice($opTemp[$op], 0, $pos+1),
				$ite,
				array_slice($opTemp[$op], $pos)
			);

			$this->_data = str_replace('\\', '',json_encode($opTemp, JSON_PRETTY_PRINT));
			$this->write(true);
		}
		else
			$success = false;

		$this->installArraySuccess[] = ['add_theme_config', $op . '; ' . $after . '; ' . $item, $success];

	}

	/**
	 * Remplaza una sección por otra.
	 * 
	 * Solo funcionará correctamente si hay un archivo cargado con la función loadFile.
	 * 
	 * @param string $search Sección a buscar (Debe ser exacta a como esta en el archivo).
	 * @param string $replace Sección con la que haras el reemplazo.
	 * @return null
	 */
	public function replace($search, $replace)
	{
		if ( !$this->_install)
		{
			$this->installArray[] = ['mod_replace', [$search, $replace]];
			return false;
		}
	
		$success = false;
	
		$this->_data = str_replace($search, $replace, $this->_data, $counter);

		if ( $counter > 0 )
			$success = true;

		$this->installArraySuccess[] = ['mod_replace', $search, $success];
	}

	/**
	 * Reemplaza una sección por otra de un archivo externo.
	 * 
	 * Nota: Debes tener cuidado con que archivo editas, ya que puedes dejar inutilizable este.
	 *  
	 * @param string $file Ruta del archivo a editar.
	 * @param string $search Sección a buscar (Debe ser exacta a como esta en el archivo).
	 * @param string $replace Sección con la que haras el reemplazo.
	 * @return null
	 */
	public function replaceE($file, $search, $replace)
	{

		if ( !$this->_install)
		{
			$this->installArray[] = ['mod_replace_e', [str_replace(MAIN_PATH, '', $file), $search, $replace]];
			return false;
		}

		$this->init();

		$this->_file = $file;
		$success = false;

		if ( is_readable($this->_file) && is_file($this->_file) )
		{
			$this->_data = file_get_contents($this->_file);
			$this->_data = str_replace($search, $replace, $this->_data, $counter);

			$this->write(true);

			if ( $counter > 0 )
				$success = true;
		}

		$this->installArraySuccess[] = ['mod_replace_e', str_replace(MAIN_PATH, '', $file), $success];
	}

	/**
	 * Agrega una sección despues de la etiqueta //After
	 * 
	 * Solo funcionará correctamente si hay un archivo cargado con la función loadFile.
	 * 
	 * @param string $add Sección a agregar.
	 * @return null
	 */
	public function addAfter($add)
	{
		if ( !$this->_install)
		{
			$this->installArray[] = ['mod_after', [$add]];
			return false;
		}

		$success = false;

		$this->_data = str_replace('//After', '//After' . "\n" . $add, $this->_data, $counter);

		if ( $counter > 0 )
			$success = true;

		$this->installArraySuccess[] = ['mod_after', $add, $success];
	}

	/**
	 * Agrega una sección antes de la etiqueta //Before
	 * 
	 * Solo funcionará correctamente si hay un archivo cargado con la función loadFile.
	 * 
	 * @param string $add Sección a agregar.
	 * @return null
	 */
	public function addBefore($add)
	{
		if ( !$this->_install)
		{
			$this->installArray[] = ['mod_before', [$add]];
			return false;
		}

		$success = false;

		$this->_data = str_replace('//Before', $add . "\n" . '//Before', $this->_data, $counter);

		if ( $counter > 0 )
			$success = true;

		$this->installArraySuccess[] = ['mod_before', $add, $success];
	}

	/**
	 * Se guardan todos los cambios realizados en el archivo.
	 * 
	 * Las funciones que dependen de esta para un correcto funcionamiento son:
	 * addAfter, addBefore y replace. 
	 * Nota: Si olvidas poner esta funcion, los cambios que hagas con las funciones
	 *       citadas arriba, no surgiran efecto.
	 *       No debes poner esta función cada vez que uses unas de las funciones de arriba,
	 *       sino despues de realizar todas las que necesita el archivo.
	 * 
	 * @param boolean $external Este parametro solo se usa para la misma clase, no ponerla en true.
	 * @return null
	 */
	public function write($external = false)
	{
		if ( !$this->_install)
		{
			$this->installArray[] = ['mod_exit', []];
			return false;
		}

		$success = true;

		if ( is_writable($this->_file) && is_file($this->_file) )
		{
			file_put_contents($this->_file, $this->_data);
		}
		else
			$success = false;

		if ( !$external )
			$this->installArraySuccess[] = ['mod_exit', '', $success];

		$this->init();
	}

	/**
	 * Agrega una columna a una tabla
	 * 
	 * El parametro @parm debe ser un array con el siguiente formato:
	 *  [ 'name' => 'Nombre de la columna',
	 * 	  'type' => 'Tipo de esta columna (varchar, int, etc)',
	 *    'size' => 'Tamaño de la columna',
	 *    'default' => 'Valor por defecto de la columna'
	 *  ]
	 * 
	 * @param string $table Tabla a la cual se le agregara la columna
	 * @param array $parm Vease la descripción de esta función
	 * @return null
	 */
	public function add_column($table, $parm)
	{
		if ( !$this->_install)
		{
			$this->installArray[] = ['db_add_column', [$table, $parm]];
			return false;
		}

		if ( empty($table) )
			exit();

		if ( !array_key_exists ('name', $parm) )
		{
			echo 'Error en la instalación.';
			exit();
		}

		if ( !array_key_exists ('type', $parm) )
		{
			$parm['type'] = 'char';
		}

		if ( !array_key_exists ('size', $parm) )
		{
			$parm['size'] = '10';
		}

		if ( !array_key_exists ('default', $parm) )
		{
			$parm['default'] = 'NULL';
		}

		$query = 'ALTER TABLE' . $table . ' ADD ' . $parm['name'] . ' ' . $parm['type'] . ' (' . $parm['size'] . ') DEFAULT ' . $parm['default'] . ';';

		$this->installArraySuccess[] = ['db_add_column', $table . '; ' . $parm['name'], $this->db()->query($query)];
	}

	/**
	 * Inserta un registro a una tabla
	 * 
	 * El parametro @parm debe ser un array con el siguiente formato:
	 *  [ 'columnaDeLaTabla' => 'Valor',
	 *    'columnaDeLaTabla2' => 'Valor2',
	 *    'columnaDeLaTabla3' => 'Valor3',
	 *  ]
	 * 
	 * @param string $table Tabla a la cual se le agregara el registro
	 * @param array $parm Vease la descripción de esta función
	 * @return null
	 */
	public function insert($table, $parm)
	{
		if ( !$this->_install)
		{
			$this->installArray[] = ['db_insert', [$table, [implode(", ",array_keys($parm)), implode(", ", array_values($parm))]]];
			return false;
		}

		if ( empty($table) )
			exit();

		$columns = implode(", ",array_keys($parm));
		$values  = implode(", ", array_values($parm));

		$query = 'INSERT INTO ' . $table . ' ( ' . $columns . ' ) VALUES (' . $values . ');';

		$this->installArraySuccess[] = ['db_insert', $table . '; ' . $columns, $this->db()->query($query)];
	}

	/**
	 * Crea una tabla en la base de datos
	 * 
	 * Ejemplo del parametro $data:
	 * 'id int(3) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	 * test varchar(10) NOT NULL,
	 * text varchar(10) NOT NULL'
	 * 
	 * @param string $table Nombre de la tabla a crear
	 * @param string $data Debe contener las columnas como su tipo, tamaño, etc.
	 * @return null
	 */
	public function create_table($table, $data)
	{
		if ( !$this->_install)
		{
			$this->installArray[] = ['db_create_table', [$table, 'CREATE TABLE ' . $table . ' ('. $data . ');']];
			return false;
		}

		if ( empty($table) )
			exit();

		$query = 'CREATE TABLE ' . $table . ' (' . $data . ');';

		$this->installArraySuccess[] = ['db_create_table', $table, $this->db()->query($query)];
	}

	/**
	 * Crea un archivo si este no existe.
	 * 
	 * Esta función no es de uso de usuario o de desarrollador, 
	 * es de uso para la misma clase.
	 * Se crea un archivo con una estructura prediseñada.
	 * 
	 * @param string $filename Nombre del archivo
	 * @return null
	 */
	public function createFile($filename)
	{
		$file = pathinfo($filename);
		$data = 
		'<?php
/**
 * BXFork
 *
 * Framework basico para el desarrollo web.
 *
 * @author 		Jovani Pérez (@BlackBlex)
 * @license 	General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package 	controllers
 * @subpackage  Modifications
 *
 * ==============Information==============
 *      Filename: '. $filename .'
 *          Path: ./controllers/modifications/
 * ---------------------------------------
*/

class ' . $file['filename'] . ' extends Controller
{

	//Toda modificación se hara apartir de aquí

	//Before

	//After

}
?>';

		file_put_contents(MOD_PATH . $filename, $data);
	}

}

?>