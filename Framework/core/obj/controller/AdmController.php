<?php
/**
 * BXFork
 *
 * Framework basico para el desarrollo web.
 *
 * @author 		Jovani Pérez (@BlackBlex)
 * @license 	General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package 	Obj
 * @subpackage  Controller
 *
 * ==============Information==============
 *      Filename: AdmController.php
 *          Path: ./core/obj/controller/
 * ---------------------------------------
*/

class AdmController
{
	use Controller2;

	//More functions or override.

	protected function before()
	{
		global $_Variables, $_View;

		if ( !isset($_SESSION['flag']) && $_SESSION['flag'] != 'admin')
			$this->redirect();

		$_View->setHeader('headeradmin');
		$_View->setFooter('footeradmin');

		if ( isset($_SESSION["u_user"]) )
		{
			$this->u_id = $_SESSION["u_id"];
			$this->u_user = $_SESSION["u_user"];
			$this->u_avatar = $_SESSION["u_avatar"];
		}

		$this->menu = [
			'dashboard' => [$_Variables->getVar('lang')['paneladmin']['dashboard'], URL . 'admin/index'],
			'config' => [$_Variables->getVar('lang')['paneladmin']['config'], URL . 'admin/config'],
			'pages' => [$_Variables->getVar('lang')['paneladmin']['pages'], URL . 'admin/pages'],
			'menus' => [$_Variables->getVar('lang')['paneladmin']['menus'], URL . 'admin/menus'],
			'languages' => [$_Variables->getVar('lang')['paneladmin']['languages'], URL . 'admin/languages'],
			'packages' => [$_Variables->getVar('lang')['paneladmin']['packages'], URL . 'admin/packages'],
			'themes' => [$_Variables->getVar('lang')['paneladmin']['themes'], URL . 'admin/themes']

		];

	}
	
}

?>