<?php
/**
 * BXFork
 *
 * Framework basico para el desarrollo web.
 *
 * @author 		Jovani Pérez (@BlackBlex)
 * @license 	General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package 	Obj
 * @subpackage  Controller
 *
 * ==============Information==============
 *      Filename: Controller.php
 *          Path: ./core/obj/controller/
 * ---------------------------------------
*/

class Controller
{
	use Controller2;

	//More functions or override.

	protected function before()
	{
	}
	
}

?>