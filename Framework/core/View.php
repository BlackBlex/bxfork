<?php
/**
 * BXFork
 *
 * Framework basico para el desarrollo web.
 *
 * @author 		Jovani Pérez (@BlackBlex)
 * @license 	General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package 	Core
 *
 * ==============Information==============
 *      Filename: View.php
 *          Path: ./core/
 * ---------------------------------------
*/

class View
{
	protected $_content;
	  private $_header	= 'header';
	  private $_view		= 'index';
	  private $_footer	= 'footer';
	protected $_controller;

	public function setHeader($header)
	{
		$this->_header = $header;
	}

	public function setView($view)
	{
		$this->_view = $view;
	}

	public function setFooter($footer)
	{
		$this->_footer = $footer;
	}

	public function loadModules($modules)
	{
		if ( is_file(THEME_PATH . 'config.json') )
		{	
			if ( array_key_exists ( $this->_view , $modules ) ){
				$modulesView = $modules[$this->_view];

				foreach ($modulesView as $key => $row) {
					$aux[$key] = $row['id'];
				}
				array_multisort($aux, SORT_ASC, $modulesView);
			}
			else
				unset($modules);
		}
		else
			throw new Exception("Theme processing failed", 20);
			
		if ( isset($modules) )
		{
			ob_start();
				if( is_file(THEME_VIEW_PATH . $this->_controller->c_controller . DS . $this->_view . 'View.tpl') )
					require_once THEME_VIEW_PATH . $this->_controller->c_controller . DS . $this->_view . 'View.tpl';
				else
					throw new Exception("Theme processing failed", 21);
			$temp = ob_get_clean();

			preg_match_all('/loadmodule\.([0-9])/', $temp, $loadMoMatchs, PREG_PATTERN_ORDER);
			$loadMoMatchs[0] = $loadMoMatchs[1];
			unset($loadMoMatchs[1]);
			$loadMdule = [];

			foreach ($loadMoMatchs[0] as $key => $value) {
				foreach ($modulesView as $keys => $values) {
					if ( $values['position'] == $value)
					{
						if ( is_file(THEME_MODULES_PATH . $values['file'] . '.tpl') )
						{
							ob_start();
								require_once THEME_MODULES_PATH . $values['file'] . '.tpl';
							$tempp = ob_get_clean();
							if ( isset($loadMdule[$value]))
								$loadMdule[$value] .= $tempp;
							else
								$loadMdule[$value] = $tempp;
						}
					}
				}
			}

			/*foreach ($loadMdule as $key => $value) {
				file_put_contents(THEME_PATH . 'loadmodule.' . $key, $value);
			}*/

			return $loadMdule;
		}
	}

	public function loadMenu($menu)
	{
		foreach ($menu as $key => $val) 
		{
			foreach ($val as $keym => $value) 
			{
				$active = 'normal';
				if ( $key === $this->_controller->c_controller && $keym === $this->_view )
				{
					$active = 'active';
				}
				if ( $value['show'] == 'disabled' )
					$active = 'disabled';

				$items[] = [
					'active'  	  => $active,
					'visible' 	  => $value['show'],
					'href'	  	  => $value['href'],
					'title'	  	  => $value['title'],
					'userConnect' => $_SESSION['u_user']
				];

			}
		}

		$this->formatMenu($items);
	}

	private function formatMenu($items)
	{
		$htmlmenu = '';
		foreach ($items as $key => $value)
		{

			if ( $value['visible'] == 'user' )
			{
				if ( empty($value['userConnect']) )
					continue;
			}
			elseif ( $value['visible'] == 'visitor' )
			{
				if ( $value['userConnect'] )
					continue;
			}

			$htmlmenu .= '<li class=" ' . $active . ' "><a href="' . $value['href']  . '">' . $value['title'] . '</a></li>';
		}

		file_put_contents(THEME_PATH . 'menu', $htmlmenu);
	}

	public function loadLanguage()
	{
		global $_Variables;
		$langV = $_Variables->getVar('lang');
		$lang = file_get_contents(LANG_PATH . strtolower($_Variables->getVar('config')['options']['language']) . DS . $this->_controller->c_controller . DS . 'lang');
		$lang = html_entity_decode($lang);
		$lang = json_decode($lang, true);
		$langV[$this->_controller->c_controller] = $lang[$this->_controller->c_controller];
		$_Variables->setVar('lang', $langV);
		unset($lang);
		unset($langV);
		
		if ( isset($_Variables->getVar('lang')[$this->_controller->c_controller][$this->_view]) )
			return $_Variables->getVar('lang')[$this->_controller->c_controller][$this->_view];
		else
			return [];
	}
	
	public function render($controller)
	{
		global $_Variables, $Twig, $Help;
		
		$this->_controller = $controller;
			$dataController = [];

		foreach ($this->_controller as $key => $value) 
		{
			if ( !substr_count($key,'c_') )
				$dataController[$key] = $value;
		}

		$User = $Help->model("Users");
		$config = file_get_contents(THEME_PATH . 'config.json');
		$config = json_decode($config, true);

		$modules = $config['modules'];
		   $menu = $config['menu'];

		unset($config);

		##########################
		#  Cargamos los modulos  #
		##########################
			$moduls = $this->loadModules($modules);
			foreach ($moduls as $key => $value) {
				file_put_contents(THEME_PATH . 'loadmodule.' . $key, $value);
			}

		########################
		#  Cargamos los menus  #
		########################
			$this->loadMenu($menu);

		########################################################
		#  Cargamos la vista y establecemos funciones - Start  #
		########################################################
			############################
			#  Funcion getUrl - Start  #
			############################
				$getUrlF = new Twig_SimpleFunction('getUrl', function ($controller=false, $action=false)
					{
						global $_Variables;
						if ( !$controller )
							$controller = $_Variables->getVar('config')['options']['defaultController'];

						if ( !$action )
							$action = $_Variables->getVar('config')['options']['defaultAction'];

						$urlString = '/'.$controller.'/'.$action;

						return $_Variables->getVar('config')['site']['url'] . $urlString;
				});

				$Twig->addFunction($getUrlF);
			##########################
			#  Funcion getUrl - End  #
			##########################

			############################
			#  Funcion getVal - Start  #
			############################
				$getValF = new Twig_SimpleFunction('getVal', function ($var)
					{
						if ( isset($this->{$var}))
							return $this->{$var};
						else
							return $this->_header;
				});

				$Twig->addFunction($getValF);
			##########################
			#  Funcion getVal - End  #
			##########################

			######################
			#  Agregar más aquí  #
			######################
		
				$template = $Twig->loadTemplate($this->_controller->c_controller . DS . $this->_view . 'View.tpl');

			############################################################
			#  Cargamos el archivo de lenguaje de nuestro controlador  #
			############################################################
				$langControllerView = $this->loadLanguage();

			##################################################################
			#  Renderizamos la vista y ponemos algunas variables "Globales"  #
			##################################################################
				echo $template->render([
					'Title'				=> $_Variables->getVar('config')['site']['name'],
					'Titleshort'		=> $_Variables->getVar('config')['site']['shortname'],
					'THEME_CSS_URL'	=> THEME_CSS_URL,
					'THEME_URL'			=> THEME_URL,
					'THEME_IMAGE_URL'	=> THEME_IMAGE_URL,
					'THEME_JS_URL'		=> THEME_JS_URL,
					'URL'					=> URL,
					'themes'				=> $_Variables->getVar('themes'),
					'data'				=> $dataController,
					'timeEnd'			=> $this->_controller->c_timeEnd,
					'lang'				=> $langControllerView,
					'langGlobal'		=> $_Variables->getVar('lang')['global']
				]);

		######################################################
		#  Cargamos la vista y establecemos funciones - End  #
		######################################################

		###################################
		#	Borramos archivos loadmodule  #
		###################################
			$this->deleteFile(THEME_PATH . 'loadmodule.0');
			$this->deleteFile(THEME_PATH . 'loadmodule.1');
			$this->deleteFile(THEME_PATH . 'loadmodule.2');
			$this->deleteFile(THEME_PATH . 'loadmodule.3');
			$this->deleteFile(THEME_PATH . 'loadmodule.4');
			$this->deleteFile(THEME_PATH . 'menu');
	}

	public function getVar($var = NULL)
	{
		if(!$var)
			return get_object_vars($this->_controller);

		if ( isset($this->_controller->$var) )
			return $this->_controller->$var;
		else
			return NULL;
	}

	private function deleteFile($file)
	{
		if (is_file($file))
			unlink($file);
		else
			die();
	}

}

?>