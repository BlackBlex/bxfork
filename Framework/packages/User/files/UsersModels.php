<?php
/**
 * BXFork
 *
 * Framework basico para el desarrollo web.
 *
 * @author 		Jovani Pérez (@BlackBlex)
 * @license 	General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package 	Models
 * @category 	Modification
 * @name 		Basic users system
 *
 * ==============Information==============
 *      Filename: UsersModels.php
 *          Path: ./models/
 * ---------------------------------------
*/

class UsersModels
{
	use Database;

	private $_table;

	public function __construct()
	{
		$this->_table = 'users';
		$this->setTable($this->_table);
		$this->DBinit();
	}

	/**
	 * Obtiene los datos del usuario
	 * @param integer $id Id del usuario
	 * @return array
	 */
	public function getUser($id)
	{
		if ( is_numeric($id) )
		{
			$query= "SELECT * FROM $this->_table WHERE id='" . $id . "';";

			$user = $this->execSql($query);
			return $user;
		}
		return 'User not found';

	}

}

?>