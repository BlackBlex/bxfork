<?php
/**
 * BXFork
 *
 * Framework basico para el desarrollo web.
 *
 * @author 		Jovani Pérez (@BlackBlex)
 * @license 	General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package 	Models
 * @category 	Modification
 * @name 		Basic users system
 *
 * ==============Information==============
 *      Filename: Users.php
 *          Path: ./models/
 * ---------------------------------------
*/

class Users
{
	use Database;
	
	private $_table;
	private $_id;
	private $_user;
	private $_password;

	public function __construct($params)
	{
		$this->_table = 'users';
		$this->setTable($this->_table);
		$this->DBinit();
	}

	public function getId()
	{
		return $this->_id;
	}

	public function setId($id)
	{
		if ( is_numeric($id) )
			$this->_id = $id;
	}

	public function getUser()
	{
		return $this->_user;
	}

	public function setUser($user)
	{
		$this->_user = $user;
	}

	public function getPassword()
	{
		return $this->_password;
	}

	public function setPassword($password)
	{
		$this->_password = $password;
	}

	/**
	 * Checa que exista el usuario.
	 * @return boolean 
	 */
	public function check()
	{
		if ( !empty($this->_user) )
		{
			$user = $this->getBy('username', $this->_user);

			if ( !empty($user) )
			{
				$this->setId($user->id);
				$this->setPassword($user->password);
				return true;
			}
		}
		return false;
	}

	/**
	 * Verifica que la contraseña dada sea la del usuario.
	 * @param string $pass 
	 * @return boolean
	 */
	public function verify($pass)
	{
		if ( password_verify($pass, $this->_password) )
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Actualiza el valor de alguna columna del usuario. 
	 * @param type $column Columna a actualizar
	 * @param type $value Nuevo valor
	 * @return boolean
	 */
	public function update($column, $value)
	{
		if ( is_numeric($this->_id) )
		{
			$query='UPDATE ' . $this->_table . ' set ' . $column . "='" . $value . "' WHERE id=" . $this->_id . ';';

			$insert = $this->db()->query($query);
			return $insert;
		}
		
	}

	/**
	 * Inserta un nuevo usuario.
	 * 
	 * Nota: No se use si ya se encuentra el usuario registrado.
	 * 
	 * @return boolean
	 */
	public function insert()
	{
		$query='INSERT INTO ' . $this->_table . ' (id, username, password) VALUES (NULL, ' . "'" . $this->_user .  "','" . password_hash($this->_password, PASSWORD_DEFAULT) . "');";

		$insert = $this->db()->query($query);

		return $insert;
	}

}

?>