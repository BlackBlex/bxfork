<?php
/**
 * BXFork
 *
 * Framework basico para el desarrollo web.
 *
 * @author 		Jovani Pérez (@BlackBlex)
 * @license 	General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package 	Main
 *
 * ==============Information==============
 *      Filename: index.php
 *          Path: ./
 * ---------------------------------------
*/

###################################################################
#  Establecemos CONSTANTES y algun tipo de configuración - Start  #
###################################################################
	set_time_limit(60);
	error_reporting(E_ALL);
	date_default_timezone_set('America/Mexico_City');
	session_start();
	define('START'				, microtime(true));
	define('DS'					, DIRECTORY_SEPARATOR);
	define('MAIN_PATH'			, realpath(dirname(__FILE__)) . DS);
	define('CONTROLLER_PATH'	, MAIN_PATH . 'controllers' . DS);
	define('MOD_PATH'			, CONTROLLER_PATH . 'modifications' . DS);
	define('CORE_PATH'			, MAIN_PATH . 'core' . DS);
	define('LANG_PATH'			, CORE_PATH . 'lang' . DS);
	define('LIB_PATH'			, CORE_PATH . 'libs' . DS);
	define('OBJ_PATH'			, CORE_PATH . 'obj' . DS);
	define('RESOURCE_PATH'		, CORE_PATH . 'resources' . DS);
	define('MODELS_PATH'		, MAIN_PATH . 'models' . DS);
	define('PACKAGE_PATH' 		, MAIN_PATH . 'packages' . DS);
	define('THEMES_PATH'   		, MAIN_PATH . 'themes' . DS);
	define('VERSION_APP'		, '0.1.1a');

#################################################################
#  Establecemos CONSTANTES y algun tipo de configuración - End  #
#################################################################



####################################
#  Funciones de autocarga - Start  #
####################################
	function loadCore($core)
	{
		$special = [ 
			'Connection' => 'database',
			'Controller2'=> 'controller',
			'AdmController'=> 'controller',
		];

		if ( file_exists(OBJ_PATH . strtolower($core) . DS . $core . '.php') )
		{
			require_once OBJ_PATH . strtolower($core) . DS . $core . '.php';
		}
		else if ( array_key_exists($core, $special) )
		{
			if ( file_exists(OBJ_PATH . $special[$core] . DS . $core . '.php') )
				require_once OBJ_PATH . $special[$core] . DS . $core . '.php';
		}
		else if ( file_exists(CORE_PATH . $core . '.php') )
		{
			require_once CORE_PATH . $core . '.php';			
		}

	}

	function loadLibs($lib)
	{
		if(file_exists(LIB_PATH . $lib . '.php'))
			require_once LIB_PATH . $lib . '.php';
	}

	spl_autoload_register('loadCore');
	spl_autoload_register('loadLibs');

##################################
#  Funciones de autocarga - End  #
##################################



###########################################################
#  Establecemos funciones de error personalizado - Start  #
###########################################################
	function errorFunction($errno, $errstr,$error_file = false,$error_line = false)
	{
		global $_Variables, $_Functions;

		if ( !$error_file )
		{
			$d = debug_backtrace();
			$error_file = $d[0]['file'];
			$error_line = -1;
		}

		if (null !== $_Variables->getVar('errors'))
			$more = $_Variables->getVar('errors');
		else
			$more = '';
		$_Variables->setVar('errors', $more . '<br>' .
			'<b>Error numero:</b> [' . $errno . '] <br>' .
			'<b>Error:</b>' . $errstr . '<br>' .
			'<b>Archivo:</b>' . str_replace(MAIN_PATH, '', $error_file) . '<br>' .
			'<b>Linea:</b>' . $error_line . '<br>' .
			'<b>Linea del error:</b>' . htmlentities(trim(file($error_file)[$error_line-1])) . '<br>');

	}
	set_error_handler('errorFunction');

#########################################################
#  Establecemos funciones de error personalizado - End  #
#########################################################



##################################################
#  Establecemos variables de uso global - Start  #
##################################################
	$_Variables 	= new Variables();
	$_Startup 		= new Startup();
	$_View        	= new View();
	$Help 			= new Help();
	$allFunctions	= [];

##################################################
#  Establecemos variables de uso global - Start  #
##################################################


######################################################################
#  Cargamos el archivo de configuración y aplicamos ajustes - Start  #
######################################################################
	$config = file_get_contents(CORE_PATH . 'config' . DS . 'config.json');
	$config = json_decode($config, true);

	if (false !== $config['site']['directory'])
		$config['site']['url'] .= '/' . $config['site']['directory'];

	$config['site']['url'] = 'http://' . $config['site']['url'];

	$_Variables->setVar('config', $config);

	unset($config);

#################################################
#  Seteamos y cargamos la opción de temas  #
#################################################
	$theme = $_Variables->getVar('config')['options']['defaultTheme'];

	if ( $_Variables->getVar('config')['options']['multitheme'] )
	{
		if ( isset($_SESSION['u_theme']) )
			$theme = $_SESSION['u_theme'];
	}

#########################################
#  Establecemos las rutas para el tema  #
#########################################
	if ( is_dir(THEMES_PATH . $theme . DS) )
		define('THEME_PATH'		, THEMES_PATH . $theme . DS);
	else
		define('THEME_PATH'		, THEMES_PATH . 'default' . DS);

	define('THEME_CSS_PATH'		, THEME_PATH . 'css' . DS);
	define('THEME_IMAGE_PATH'	, THEME_PATH . 'img' . DS);
	define('THEME_JS_PATH'		, THEME_PATH . 'js' . DS);
	define('THEME_MODULES_PATH' , THEME_PATH . 'modules' . DS);
	define('THEME_HEADER_PATH'	, THEME_PATH . 'headers' . DS);
	define('THEME_FOOTER_PATH'	, THEME_PATH . 'footers' . DS);
	define('THEME_VIEW_PATH'	, THEME_PATH . 'views' . DS);

	define('URL'				, $_Variables->getVar('config')['site']['url'] . '/');
	define('THEME_URL'			, URL . 'themes/' . $theme . '/');
	define('THEME_CSS_URL'		, THEME_URL . 'css' . '/');
	define('THEME_IMAGE_URL'	, THEME_URL . 'img' . '/');
	define('THEME_JS_URL'		, THEME_URL . 'js' . '/');

##############################
#  Hacemos la carga de Twig  #
##############################
	require_once LIB_PATH . 'Twig'. DS . 'Autoloader.php';
	Twig_Autoloader::register();
	$loader = new Twig_Loader_Filesystem([THEME_PATH,THEME_VIEW_PATH,THEME_MODULES_PATH,THEME_HEADER_PATH,THEME_FOOTER_PATH]);
	$Twig = new Twig_Environment($loader, array(
		'cache' => 'cache',
		'debug' => $_Variables->getVar('config')['options']['displayErrors']
	));

#########################################################
#  Cargamos el archivo del idioma que esta por defecto  #
#########################################################
	$lang = file_get_contents(LANG_PATH . strtolower($_Variables->getVar('config')['options']['language']) . DS . 'lang');
	$lang = html_entity_decode($lang);
	$lang = json_decode($lang, true);
	$_Variables->setVar('lang', $lang);
	unset($lang);

###############################################################
#  Iniciamos a la carga de toda clase/función/metodo - Start  #
###############################################################
	try
	{
		$Help->loadModel("UsersModels");
		$Help->loadModel("Users");
		################################################################################
		#  Llamamos a la función que se encarga de procesar todos los datos obtenidos  #
		################################################################################
			$_View->render($_Startup->load());
	}
	catch(Exception $e)
	{
		if ( true === $_Variables->getVar('config')['options']['displayErrors'] )
		{
			http_response_code(400);
			echo 	'<pre>',
					'<b>Error numero:</b> [' . $e->getCode() . '] <br>',
					'<b>Error:</b> ' . $e->getMessage() . '<br>',
					'<b>Archivo:</b> ' . str_replace(MAIN_PATH, '', $e->getFile()) . '<br>',
					'<b>Linea:</b> ' . $e->getLine() . '</pre><br>';
		}

	}
#############################################################
#  Iniciamos a la carga de toda clase/función/metodo - End  #
#############################################################



###################################################################
#  ¿Hay errores? muestralos (Si esta activado la opción) - Start  #
###################################################################
	if ( true === $_Variables->getVar('config')['options']['displayErrors'] && !empty($_Variables->getVar('errors')))
		echo '<pre>', $_Variables->getVar('errors'), '</pre>';
#################################################################
#  ¿Hay errores? muestralos (Si esta activado la opción) - End  #
#################################################################


?>