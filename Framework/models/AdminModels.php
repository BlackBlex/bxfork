<?php
/**
 * BXFork
 *
 * Framework basico para el desarrollo web.
 *
 * @author 		Jovani Pérez (@BlackBlex)
 * @license 	General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package 	Models
 *
 * ==============Information==============
 *      Filename: AdminModels.php
 *          Path: ./models/
 * ---------------------------------------
*/

class AdminModels
{
	use Database;

	private $_table;

	public function __construct()
	{
		$this->DBinit();
	}

	/**
	 * Obtiene el total de usuarios registrados.
	 * @return integer
	 */
	public function getRegisteredUsers()
	{
		$this->_table = 'users';
		$this->setTable($this->_table);
		$user = $this->getAll();

		$user = count($user);

		return $user;
	}


	/**
	 * Obtiene el ultimo usuario registrado.
	 * @return name
	 */
	public function getLastUser()
	{
		$this->_table = 'users';
		$this->setTable($this->_table);
		$user = $this->getAll();

		$user = $user[count($user)-1];

		return $user->username;
	}

}

?>