<?php
/**
 * BXFork
 *
 * Framework basico para el desarrollo web.
 *
 * @author 		Jovani Pérez (@BlackBlex)
 * @license 	General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package 	Models
 *
 * ==============Information==============
 *      Filename: InstallModels.php
 *          Path: ./models/
 * ---------------------------------------
*/

class InstallModels
{
	use Database;
}

?>