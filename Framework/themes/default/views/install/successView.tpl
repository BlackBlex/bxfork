{% include getVal("_header") ~ '.tpl' %}
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8 box_well">
				<section>
					<div class="panel panel-success">
						<div class="panel-heading">
							<h3 class="panel-title">{{ lang.success }}</h3>
						</div>
						<div class="panel-body">

							{% for change in data.changes if change.2 == true %}
								<table class= 'info'>
									<tbody>
										<tr>
											<td align="right">{{ attribute(lang, change.0) }}</td>
											<td>{{ change.1 }}</td>
										</tr>
									</tbody>
								</table>
							{% endfor %}

						</div>
					</div>
				</section>
				<section>
					<div class="panel panel-danger">
						<div class="panel-heading">
							<h3 class="panel-title">{{ lang.failed }}</h3>
						</div>
						<div class="panel-body">

							{% for change in data.changes if change.2 == false %}
								<table class= 'info'>
									<tbody>
										<tr>
											<td align="right">{{ attribute(lang, change.0) }}</td>
											<td>{{ change.1 }}</td>
										</tr>
									</tbody>
								</table>
							{% endfor %}

						</div>
					</div>
				</section>
			</div>
			<div class="col-md-2"></div>
		</div>
{% include getVal("_footer") ~ '.tpl' %}