<!DOCTYPE HTML>
	<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>{{ Titleshort }}</title>
		<link rel="stylesheet" type="text/css" href="{{ THEME_CSS_URL }}bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="{{ THEME_CSS_URL }}style.css"/>
		<!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'> -->
		<script type="text/javascript" src="{{ THEME_JS_URL }}jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="{{ THEME_JS_URL }}bootstrap.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
	</head>
	<body>
		<header>
			<nav class="navbar navbar-inverse" style="background-color:blue">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#items_de_navbar">
							<span class="sr-only">Navegación</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand title" href="#">{{ Titleshort }}</a>
					</div>
					<div class="collapse navbar-collapse navbar-right" id="items_de_navbar">
						<ul class="nav navbar-nav">
							{% include 'menu' ignore missing %}
						</ul>
					</div>
				</div>
			</nav>
		</header>
		<main class="container">