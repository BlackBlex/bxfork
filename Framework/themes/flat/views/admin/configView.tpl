{% include getVal("_header") ~ '.tpl' %}
		<form class="form-horizontal" _lpchecked="1" action="{{ getUrl('admin', 'config') }}" method="post">
			<div class="row">
				<div class="col-md-12 box_trans">

					{% if data.message is not empty %}
						{% if data.message == "f1" %}
							<div class="alert alert-success">
								<strong>¡{{ lang.success }}!</strong> {{ lang.success1 }}
							</div>
						{% else %}
							<div class="alert alert-danger">
								<strong>¡{{ lang.error }}!</strong> {{ lang.error1 }}
							</div>
						{% endif %}
					{% endif %}
					<div class="col-md-6 box_white">
						<fieldset>
							<legend>
								<h2>
									<i class="fa fa-pencil-square-o" aria-hidden="true"></i>    {{ lang.configsite }}
								</h2>
							</legend>

							{% for key, var in data.config_site %}
								<div class="form-group size-17">
									<label for="inputsite{{ key }}" class="col-lg-4 control-label">{{ attribute(lang, key) }}</label>
									<div class="col-lg-8">
										<input type="text" class="form-control" id="inputsite{{ key }}" name="inputsite{{ key }}" placeholder="{{ attribute(lang, key) }}" autocomplete="off" value="{{ var }}">
										{% if attribute(lang, key~'s') %}
											<span class="help-block">{{ attribute(lang, key~'s') }}</span>
										{% endif %}
									</div>
								</div>
							{% endfor %}
						</fieldset>
					</div>

					<div class="col-md-6 box_white">
						<fieldset>
							<legend>
								<h2>
									<i class="fa fa-pencil-square-o" aria-hidden="true"></i>    {{ lang.configoptions }}
								</h2>
							</legend>

							{% for key, var in data.config_options %}
								<div class="form-group size-17">
									{% if var == "1" or var == "0" %}
									{% else %}
										<label for="inputoptions{{ key }}" class="col-lg-4 control-label">{{ attribute(lang, key) }}</label>
									{% endif %}
										{% if var == "1" or var == "0" %}
											<div class="col-lg-12">
													<div class="checkbox">
														<center>
															<label>
																<input type='hidden' value='off' id="inputoptions{{ key }}" name="inputoptions{{ key }}">
																{% if var %}
																	<input type="checkbox" id="inputoptions{{ key }}" name="inputoptions{{ key }}" checked="{{ var }}">{{ attribute(lang, key) }}
																{% else %}
																	<input type="checkbox" id="inputoptions{{ key }}" name="inputoptions{{ key }}">{{ attribute(lang, key) }}
																{% endif %}
															</label>
														</center>
													</div>
											</div>
											<label class="col-lg-4 control-label"></label>
											<div class="col-lg-8">
												<span class="help-block">{{ attribute(lang, key~'s') }}</span>
											</div>
										{% else %}
											<div class="col-lg-8">
												{% if key == 'language' or key == 'defaultTheme' %}
													<select class="form-control" id="inputoptions{{ key }}" name="inputoptions{{ key }}">
														{% for value in attribute(data,key) %}
															{% if ( value == var ) %}
																<option selected value="{{value}}">{{ value }}</option>
															{% else %}
																<option value="{{value}}">{{ value }}</option>
															{% endif %}
														{% endfor %}
													</select>
												{% else %}
													<input type="text" class="form-control" id="inputoptions{{ key }}" name="inputoptions{{ key }}" placeholder="{{ attribute(lang, key) }}" autocomplete="off" value="{{ var }}" >
												{% endif %}
												<span class="help-block">{{ attribute(lang, key~'s') }}</span>
											</div>
										{% endif %}
								</div>
							{% endfor %}
						</fieldset>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 box_trans">
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 box_trans">
					<div class="col-md-3 box_trans">
						
					</div>
					<div class="col-md-6 box_white">
						<fieldset>
							<legend>
								<h2>
									<i class="fa fa-pencil-square-o" aria-hidden="true"></i>    {{ lang.configdatabase }}
								</h2>
							</legend>

							{% for key, var in data.config_database %}
								<div class="form-group size-17">
									<label for="inputdatabase{{ key }}" class="col-lg-4 control-label">{{ attribute(lang, key) }}</label>
									<div class="col-lg-8">
										<input type="text" class="form-control" id="inputdatabase{{ key }}" name="inputdatabase{{ key }}" placeholder="{{ attribute(lang, key) }}" autocomplete="off" value="{{ var }}">
										{% if attribute(lang, key~'s') %}
											<span class="help-block">{{ attribute(lang, key~'s') }}</span>
										{% endif %}
									</div>
								</div>
							{% endfor %}
						</fieldset>
					</div>
					<div class="col-md-3 box_trans">
						
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 box_trans">
					<div class="col-md-3 box_trans">
						
					</div>
					<div class="col-md-6 box_trans">
						<div class="form-group">
							<center>
								<button type="submit" id="save" name="save" class="btn btn-success btn-lg">{{ lang.savechanges }}</button>
							</center>
						</div>
					</div>
					<div class="col-md-3 box_trans">
						
					</div>
				</div>
			</div>
		</form>
{% include getVal("_footer") ~ '.tpl' %}