{% include getVal("_header") ~ '.tpl' %}
		<div class="row">
			<div class="col-md-12 box_trans">
				<div class="col-md-2 box_trans">
				</div>

				<div class="col-md-8 box_trans">
					<div class="col-md-12 box_trans">
						<div class="box_lightgreen">
							<div class="verticalAlign">
								<i class="fa fa-file fa-4x" aria-hidden="true"></i>
								<div class="verticalLine">
									<span class="box_title">{{ lang.add }}</span>
								</div>
							</div>
						</div>
						<div class="box_border_lightgreen">
							<form class="form-horizontal" _lpchecked="1" action="{{ getUrl('admin', 'menus/addm') }}" method="post">
								<fieldset>
									<div class="form-group size-17">
										<label for="namesection" class="col-lg-4 control-label">{{ lang.addsection }}</label>
										<div class="col-lg-8">
											<input type="text" class="form-control" id="namesection" name="namesection" placeholder="{{ lang.addsection }}" autocomplete="off">
										</div>
									</div>
									<center><h3>{{ lang.first }}</h3></center>
									<div class="form-group size-17">
										<label for="nameitem" class="col-lg-4 control-label">{{ lang.addname }}</label>
										<div class="col-lg-8">
											<input type="text" class="form-control" id="nameitem" name="nameitem" placeholder="{{ lang.addname }}" autocomplete="off">
										</div>
									</div>
									<div class="form-group size-17">
										<label for="titleitem" class="col-lg-4 control-label">{{ lang.addtitle }}</label>
										<div class="col-lg-8">
											<input type="text" class="form-control" id="titleitem" name="titleitem" placeholder="{{ lang.addtitle }}" autocomplete="off">
										</div>
									</div>
									<div class="form-group size-17">
										<label for="hrefitem" class="col-lg-4 control-label">{{ lang.addhref }}</label>
										<div class="col-lg-8">
											<input type="text" class="form-control" id="hrefitem" name="hrefitem" placeholder="{{ lang.addhref }}" autocomplete="off">
										</div>
									</div>
									<div class="form-group size-17">
										<label for="showitem" class="col-lg-4 control-label">{{ lang.addshow }}</label>
										<div class="col-lg-8">
											<input type="text" class="form-control" id="showitem" name="showitem" placeholder="{{ lang.addshow }}" autocomplete="off">
										</div>
									</div>

									<div class="form-group">
										<center>
											<button type="submit" id="save" name="save" class="btn btn-primary btn-lg">{{ lang.savechanges }}</button>
										</center>
									</div>
								</fieldset>
							</form>
						</div>
					</div>
				</div>

				<div class="col-md-2 box_trans">
				</div>
			</div>
		</div>
{% include getVal("_footer") ~ '.tpl' %}