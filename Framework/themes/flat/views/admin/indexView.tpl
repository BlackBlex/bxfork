{% include getVal("_header") ~ '.tpl' %}
		<div class="row">
			<div class="col-md-12 box_trans">
				<div class="col-md-4 box_trans">
					<div class="col-md-12 box_red">
						<div class="verticalAlign">
							<i class="fa fa-users fa-4x" aria-hidden="true"></i>
							<div class="verticalLine">
								<span class="box_title">{{ data.countUsers }}</span>
								<br>
								<span class="box_subtitle">{{ lang.countusers }}</span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 box_trans">
					<div class="col-md-12 box_lightblue">
						<div class="verticalAlign">
							<i class="fa fa-user fa-4x" aria-hidden="true"></i>
							<div class="verticalLine">
								<span class="box_title">{{ data.lastUser }}</span>
								<br>
								<span class="box_subtitle">{{ lang.lastuser }}</span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 box_trans">
					<div class="col-md-12 box_lightgreen">
						<div class="verticalAlign">
							<i class="fa fa-flag fa-4x" aria-hidden="true"></i>
							<div class="verticalLine">
								<span class="box_title">{{ data.version }}</span>
								<br>
								<span class="box_subtitle">{{ lang.version }}</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 box_trans">

			</div>
		</div>
		<div class="row">
			<div class="col-md-12 box_trans">

			</div>
		</div>
		<div class="row">
			<div class="col-md-12 box_trans">

			</div>
		</div>
{% include getVal("_footer") ~ '.tpl' %}