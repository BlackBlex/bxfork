{% include getVal("_header") ~ '.tpl' %}
				{% if data.result is not empty %}
					{% if data.result == "f1" %}
						<div class="alert alert-success">
							<strong>¡{{ lang.success }}!</strong> {{ lang.success1 }}
						</div>
					{% else %}
						<div class="alert alert-danger">
							<strong>¡{{ lang.error }}!</strong> {{ lang.error1 }}
						</div>
					{% endif %}
				{% endif %}
				
				{% for key, var in data.items %}
					<div class="row">
						<div class="col-md-12 box_trans">
							<h1>{{ key }} &nbsp;&nbsp; <a href="{{ getUrl('admin', 'menus/add/' ~ key  ) }}" title="{{ lang.add ~ key }}" style="color:green;"><i class="fa fa-plus" aria-hidden="true"></i></a></h1>
							<hr>
							{% for val in var %}
								<a href="{{ getUrl('admin', 'menus/' ~ key ~ '/' ~ val.0  ) }}" class="col-md-4 pages">
									<div class="col-md-12 box_green">
										{{ val.0 }}
									</div>
								</a>
							{% endfor %}
						</div>
					</div>
				{% endfor %}
				<hr>
				<div class="row">
					<div class="col-md-12 box_trans">
						<center>
							<a style="color:#569e7b;" href="{{ getUrl('admin', 'menus/addm' ) }}" title="{{ lang.addm }}" class="fa-stack fa-lg box_title">
								<i class="fa fa-circle-o fa-stack-2x"></i>
								<i class="fa fa-plus fa-stack-1x"></i>
							</a>
						</center>
					</div>
				</div>

{% include getVal("_footer") ~ '.tpl' %}