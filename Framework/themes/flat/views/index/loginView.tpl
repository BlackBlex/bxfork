{% include getVal("_header") ~ '.tpl' %}
		
		<div class="row">
			<div class="col-md-3 trans">
			</div>
			<div class="col-md-6 box_gray">
				{% if data.message is not empty %}
					<div class="alert alert-danger" role="alert">{{ attribute(lang, data.message) }}</div>
				{% endif %}
				<form action='{{ getUrl("user","login") }}' method="post" >
					<div class="c_blue text-center">
						<h3><i class="fa fa-users" aria-hidden="true"></i>  {{ lang.login }}</h3>
					</div>
					<br>
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1"><i class="fa fa-user" aria-hidden="true">&nbsp;</i></span>
						<input type="text" placeholder="{{ lang.user }}" name="user" class="form-control" aria-describedby="basic-addon1">
					</div>
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon2"><i class="fa fa-key" aria-hidden="true"></i></span>
						<input type="password" placeholder="{{ lang.pass }}" name="pass" class="form-control" aria-describedby="basic-addon2">
					</div>
					<br>
					<input type="submit" value="{{ lang.send }}" class="btn btn-success btn-lg pull-right"/>
				</form>
			</div>
			<div class="col-md-3 gray">

			</div>
		</div>
{% include getVal("_footer") ~ '.tpl' %}