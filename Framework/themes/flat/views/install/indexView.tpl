{% include getVal("_header") ~ '.tpl' %}
		<div class="row">
			<div class="col-md-8 box_well">
				<section>
					<div class="c_blue text-center">
						<h2>{{ lang.changes }}</h2>
					</div>
					{% for change in data.actions %}
						<div class="center {{ change.0 }}">
							{{ attribute(lang, change.0) }}

							{% if change.0 == "mod_file" %}
								{{ attribute(change.1, 0) }}
							{% elseif change.0 == "add_route" %}
								<table>
									<tbody>
										<tr>
											<td align="right">								
												{{ lang.add_route2 }}
											</td>
											<td>							
												{{ attribute(change.1, 0) }}
											</td>
										</tr>
										<tr>
											<td align="right">						
												{{ lang.add_route3 }}
											</td>
											<td>
												{{ attribute(change.1, 1) }}
											</td>
										</tr>
									</tbody>
								</table>
							{% elseif change.0 == "add_lang" or change.0 == "add_theme_config" %}
								<table>
									<tbody>
										<tr>
											<td align="right">
												{{ attribute(lang, change.0 ~ 2) }}
											</td>
											<td>							
												{{ attribute(change.1, 0) }}
											</td>
										</tr>
										<tr>
											<td align="right">
												{{ attribute(lang, change.0 ~ 3) }}
											</td>
											<td>
												{{ attribute(change.1, 1) }}
											</td>
										</tr>
										<tr>
											<td align="right">	
												{{ attribute(lang, change.0 ~ 4) }}
											</td>
											<td>
												{{ attribute(change.1, 2) }}
											</td>
										</tr>
										<tr>
											<td align="right">
												{{ attribute(lang, change.0 ~ 5) }}
											</td>
										</tr>
									</tbody>
								</table>
								<pre><code class="json">{{ attribute(change.1, 3) }}</code></pre>
							{% elseif change.0 == "mod_before" or change.0 == "mod_after" %}
								<br>
								<div>
									<pre><code class="php">{{ attribute(change.1, 0) }}</code></pre>
								</div>
							{% elseif change.0 == "mod_replace" %}
								<div>
									<pre><code class="php">{{ attribute(change.1, 0) }}</code></pre>
								</div>

								{{ lang.mod_replace2 }}

								<div>
									<pre><code class="php">{{ attribute(change.1, 1) }}</code></pre>
								</div>

							{% elseif change.0 == "mod_replace_e" %}
								<table>
									<tbody>
										<tr>
											<td align="right">								
												{{ attribute(lang, change.0 ~ 2) }}
											</td>
											<td>
												{{ attribute(change.1, 0) }}
											</td>
										</tr>
									</tbody>
								</table>
								{{ attribute(lang, change.0 ~ 3) }}
								<div>
									<pre><code class="php">{{ attribute(change.1, 1) }}</code></pre>
								</div>
								{{ attribute(lang, change.0 ~ 4) }}
								<div>
									<pre><code class="php">{{ attribute(change.1, 2) }}</code></pre>
								</div>
							{% elseif change.0 == "mod_move_file" %}
								<table>
									<tbody>
										<tr>
											<td align="right">								
												{{ attribute(lang, change.0 ~ 2) }}
											</td>
											<td>							
												{{ attribute(change.1, 0) }}
											</td>
										</tr>
										<tr>
											<td align="right">
												{{ attribute(lang, change.0 ~ 3) }}
											</td>
											<td>
												{{ attribute(change.1, 1) }}
											</td>
										</tr>
									</tbody>
								</table>

							{% elseif change.0 == "db_add_column" or change.0 == "db_insert" %}
								<table>
									<tbody>
										<tr>
											<td align="right">								
												{{ attribute(lang, change.0 ~ 2) }}
											</td>
											<td>							
												{{ attribute(change.1, 0) }}
											</td>
										</tr>
										<tr>
											<td align="right">
												{{ attribute(lang, change.0 ~ 3) }}
											</td>
											<td>
												{{ attribute(attribute(change.1, 1), 'name') ~ attribute(attribute(change.1, 1), 0) }}
											</td>
										</tr>
										<tr>
											<td align="right">
												{{ attribute(lang, change.0 ~ 4) }}
											</td>
											<td>
												{{ attribute(attribute(change.1, 1), 'type') ~ attribute(attribute(change.1, 1), 1) }}
											</td>
										</tr>
									</tbody>
								</table>
							{% elseif change.0 == "db_create_table" %}
								<table>
									<tbody>
										<tr>
											<td align="right">								
												{{ attribute(lang, change.0 ~ 2) }}
											</td>
											<td>							
												{{ attribute(change.1, 0) }}
											</td>
										</tr>
										<tr>
											<td align="right">
												{{ attribute(lang, change.0 ~ 3) }}
											</td>
										</tr>
									</tbody>
								</table>
								<pre><code class="sql">{{ attribute(change.1, 1) }}</code></pre>
							{% endif %}
						</div>
					{% endfor %}
					<br>
					<br>
					<a href="{{ getUrl('install','success') ~ '/' ~ data.mod }}" class="buttonInstall center btn btn-success">{{ lang.install }}</a>
				</section>
			</div>			
			<div class="col-md-4">
				<section>
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title">{{ lang.info }}</h3>
						</div>
						<div class="panel-body">
							<table class='info'>
								<tbody>
									<tr>
										<td align="right">{{ lang.name }}</td>
										<td>{{ data.name }}</td>
									</tr>
									<tr>
										<td align="right">{{ lang.desc }}</td>
										<td>{{ data.desc }}</td>
									</tr>
									<tr>
										<td align="right">{{ lang.author }}</td>
										<td>{{ data.author }}</td>
									</tr>
									<tr>
										<td align="right">{{ lang.version }}</td>
										<td>{{ data.version }}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</section>
			</div>
		</div>
{% include getVal("_footer") ~ '.tpl' %}