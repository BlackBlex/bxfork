
**BXFork | Framework básico**
===================

BXFork contendrá numerosas características, las cuales se añadirán conforme vaya avanzando en el desarrollo.

------
Esta realizado con:

- Lenguage: **PHP**
- Diseño: **Bootstrap 3**, **highlight.js**
- Template engine: **Twig**

------

***Quiero aclarar que esta versión, no es una versión operativa o final, se puede empezar a trastear como funciona y todo, hasta crear paquetes, pero no es recomendable para realizar un proyecto tomando de base esta.**

------

Características de la versión:

- Sistema multilenguaje
- Sistema multitema
- Acceso a Base de Datos
- Instalador de módulos o paquetes
- Sistemas de rutas URL (Esto se complementa con los paquetes, ya que puede que haya algún paquete que de una funcionalidad extra a algún controlador, y para evitar reemplazar dicho controlador, se crea uno nuevo que contiene dicha modificación o modificaciones)

------

#**Imágenes**

Diseño actual:
![Versión actual](Imagenes/BxFork_home.png)
![Versión actual](Imagenes/BxFork_about.png)
![Versión actual](Imagenes/instalacion_de_paquete.png)

**Instalación**
===================
1. Descargamos una copia de BxFork que está en la carpeta Framework.
2. Descomprimimos el archivo en nuestra carpeta **htdocs**, **www**, **www_root**, **etc** (Sea nuestro caso). | *Se puede poner en una subcarpeta, no necesariamente debe estar en la raíz.*
3. Nos dirigimos a la carpeta **core** y entramos a **config**, una vez ahí abrimos el archivo **config.json** con nuestro editor de código que usemos.
Editamos las siguientes lineas:
En la sección de **site**
**url** Aquí pondremos la dirección de nuestro sitio (*Sin http, ni "/" al final*)
**directory** Si pusimos los archivos en una subcarpeta, pon el nombre de la carpeta (*Sin "/" al principio, ni al final*)
En la sección de **database**
**server** Aquí pondremos la dirección del servidor MySQL.
**name** Nombre de la base de datos a usar.
**user** Usuario del servidor MySQL.
**pass** Contraseña del servidor MySQL.
4. Vamos a nuestro programa de gestor de base de datos, o al **phpmyadmin**, y creamos una nueva base de datos con el nombre puesto en el paso anterior, con un cotejamiento en **utf8_general_ci**
5. Vamos a nuestro navegador y ingresamos a la dirección puesta en el archivo **config.json**, si todo salio bien, veremos la pagina como la primera imagen puesta arriba.

**Documentación ~ (En construcción)**
===================
BXFork cuenta con las siguientes constantes:

- **URL** contiene la dirección del sitio.
- **MAIN_PATH** contiene la ruta raiz del proyecto.
- **CONTROLLER_PATH** contiene la ruta a la carpeta de los controladores.
- **MOD_PATH** contiene la ruta a la carpeta de los controladores modificados.
- **CORE_PATH** contiene la ruta a la carpeta core.
- **LANG_PATH** contiene la ruta a la carpeta de los lenguajes.
- **LIB_PATH** contiene la ruta a la carpeta de las librerías "externas".
- **OBJ_PATH** contiene la ruta a la carpeta de todos los objetos que pueden ser utilizados por los controladores, y modelos.
- **MODELS_PATH** contiene la ruta a la carpeta de los modelos.
- **PACKAGE_PATH** contiene la ruta a la carpeta de los paquetes que se pueden instalar.
- **THEMES_PATH** contiene la ruta a la carpeta de los temas.
- **MODELS_PATH** contiene la ruta a la carpeta de los modelos.

Para el caso del tema que este configurado hay las siguientes constantes:

- **THEME_PATH** contiene la ruta a la carpeta del tema configurado por defecto en el archivo **config.json**.
- **THEME_CSS_PATH** contiene la ruta a la carpeta css del tema.
- **THEME_IMAGE_PATH** contiene la ruta a la carpeta img del tema.
- **THEME_JS_PATH** contiene la ruta a la carpeta js del tema.
- **THEME_HEADER_PATH** contiene la ruta a la carpeta headers del tema.
- **THEME_FOOTER_PATH** contiene la ruta a la carpeta footers del tema.
- **THEME_VIEW_PATH** contiene la ruta a la carpeta views del tema.
- **THEME_URL** contiene la dirección de la carpeta del tema usado actualmente.
- **THEME_CSS_URL** contiene la dirección de la carpeta css del tema.
- **THEME_IMAGE_URL** contiene la dirección de la carpeta img del tema.
- **THEME_JS_URL** contiene la dirección de la carpeta js del tema.

**¡Como comenzar!**
===================
Primero que nada debemos tener en cuenta lo siguiente.

El controlador esta acompañado de una carpeta con el mismo nombre en la carpeta views del tema que estemos usando, o de los temas disponibles de su sitio web.

Ejemplo:

Tengo el controlador **Prueba** (*PruebaController.php*) en la carpeta **controllers**
En mi tema **default** dentro de la carpeta views deberé de tener una carpeta llamada **prueba** y dentro de esta estarán las vistas.

**Imagen de como debe quedar:**

![Imagen ejemplo](Imagenes/Controlador_y_vista.png)

------

**Como hacer correctamente la url hacia un controlador y su acción con sus parámetros**

Primero que nada debemos tener en cuenta como se conforma la url.

La url esta compuesta de 3 secciones:

- 1° sección: **el controlador**
- 2° sección: **la acción**
- 3° sección: **los argumentos**

En la sección de los argumentos, estos deben seguir la misma estructura que las demás secciones, osea con **/**, si son 2 parámetros (/**par1**/**par2**), si son 3 parámetros (/**par1**/**par2**/**par3**), si son n parámetros (/**par1**/**....**/**par(n-1)**/**parn**) y así sucesivamente.

Ejemplo:
[MiSitio/](http://misitioweb.com)**index**/**login**/**BlackBlex**/**Test123**

En la url de arriba llamamos al controlador **index** (IndexController) junto a su acción **login** pasando 2 parámetros, los cuales son: **BlackBlex** y **Test123**.

**Nota:** para que funcione correctamente la acción **login** debe de recibir 2 parámetros, sino nos mostrará **error 13**, esto se debe a que por defecto esta activado el limite de parámetros.

**Correcto:**
```php
    public function login($usuario, $texto)
    {
        echo 'Hola ', $usuario, '<br>Texto: ', $texto;
    }
```

**Error 13:**
```php
    public function login($usuario)
    {
        echo 'Hola ', $usuario, '<br>Texto: ', $texto;
    }
```

O

```php
    public function login()
    {
        echo 'Hola ', $usuario, '<br>Texto: ', $texto;
    }
```

Ejemplo para construir la URL de nuestro **MathController** con la acción **suma** y este recibe **3 parámetros**:

misitio.com/**math**/**suma**/**10**/**52**/**3**

------

**Hacer una nueva pagina - En el mismo controlador**

Nos dirigimos al controlador del que queremos añadirle una nueva pagina (en la carpeta **controllers**).

En mi caso elegiré a **IndexController.php** y lo abrimos con nuestro editor:

Este es mi **IndexController.php**

```php
<?php
/**
 * BXFork
 *
 * Framework basico para el desarrollo web.
 *
 * @author      Jovani Pérez (BlackBlex)
 * @license     General Public License (GPLv3) | [url]http://www.gnu.org/licenses/[/url]
 * @package     controllers
 *
 * ==============Information==============
 *      Filename: IndexController.php
 *          Path: ./controllers/
 * ---------------------------------------
*/

class IndexController extends Controller
{

    public function index()
    {
    }

    public function about($test)
    {
        $this->test = $test;
    }

    public function theme($theme)
    {
        $_SESSION['u_theme'] = $theme;
        $this->redirect();
    }

}

?>
```

En cualquier parte de la clase añadimos el siguiente código (*Yo recomiendo que lo pongan el código abajo de las funciones ya existentes*):

**Nota:** el nombre de la función sera con el que van a acceder los usuarios así que recomiendo que el nombre sea en minúsculas y que no sea largo; En mi caso elegí **prueba**

```php
    public function prueba()
    {
    }
```

Guardamos los cambios.

Creamos un archivo con el siguiente nombre: *NombreDeLaFunción + View.tpl*

En mi caso: **pruebaView.tpl**, este archivo lo ponemos en la carpeta con el mismo nombre de nuestro controlador (En mi caso elegí **IndexController** por lo que la pondré en la carpeta **index**) dentro de la carpeta **views** que esta en el tema que estemos usando actualmente.

Este archivo debe contener el siguiente código:

```html
{% include getVal("_header") ~ '.tpl' %}

<!-- Aquí va el codigo de la pagina -->

{% include getVal("_footer") ~ '.tpl' %}
```

Así nos debe quedar:

![Ejemplo](Imagenes/hacer_nueva_pagina_en_el_mismo_controlador.png)

Para checar que esta correctamente creada la pagina, nos vamos a nuestro navegador, y ingresamos la dirección de la pagina + **/index/prueba**, en mi caso estoy en localhost, así que me dirijo a: **http://localhost/index/prueba**

Y deberíamos ver nuestra pagina prueba, esta es la mía personalizada un poco (*a ustedes les debe aparecer en blanco con el header y el footer*).
![Ejemplo](Imagenes/hacer_nueva_pagina_en_el_mismo_controlador_2.png)

------

**Hacer una nueva pagina - Con un controlador nuevo**

Nos dirigimos a la carpeta **controllers** y creamos un nuevo archivo con la siguiente estructura:

*Nombre + Controller.php*

**Nota:** la primera letra del nombre y de controller debe estar en mayúscula.

Ejemplo: **PruebaController.php**

Lo que debe contener este archivo es una clase con el mismo nombre y que extienda de Controller y una función llamada index.

Ejemplo:

```php
class PruebaController extends Controller
{

    public function index()
    {
    }

}
```

Una vez echo lo anterior, debemos crear el archivo de lenguaje para dicho controlador, nos dirigimos a la carpeta **core/lang/**, entramos en el idioma que queremos (en mi caso es: *es*) y luego creamos una carpeta llamada con el nombre que le pusimos a nuestro controlador (todo en minúsculas), adentro de esta creamos un archivo llamado "**lang**" sin ninguna extensión.

Nos vamos a la carpeta **views** que se encuentra en la carpeta de nuestro tema y creamos una nueva carpeta como hicimos en la carpeta lenguaje, adentro de ella creamos el archivo **indexView.tpl**

Y su contenido será:

```html
{% include getVal("_header") ~ '.tpl' %}

<!-- Aquí va el codigo de la pagina -->

{% include getVal("_footer") ~ '.tpl' %}
```

**Imagen de como debería quedar:**

![Ejemplo](Imagenes/hacer_nueva_pagina_en_otro_controlador.png)

Si quieres agregarle más paginas, debes seguir los pasos del anterior apartado.

------

**Pasar variables de nuestro controlador a la vista**

Abrimos el controlador con el que queremos trabajar, yo usaré el creado en el apartado anterior: **PruebaController.php**

Nos dirigimos a la acción o a la función de la que queremos pasar variables, yo lo haré en **index**

En ves de declarar la variable así:

```php
$saludo= 'Hola :D';
```

Vamos hacer como si estuviéramos accediendo a una variable de la clase con el uso de **$this**:

```php
$this->saludo= 'Hola :D';
```

Guardamos cambio, y nos vamos a la vista de la función, en mi caso **indexView.tpl**, para acceder a nuestra variable debemos hacer uso de la sintaxis de **Twig**, la cual es la siguiente:

```html
{{ nombreDeLaVariable }}
```

En nuestro caso la variable **saludo** como otras variables, esta almacenada en otra variable llamada **data**, por lo que nuestro código terminaría así:

```html
{{ data.saludo }}
```

Por lo que solo nos falta acomodarla en nuestro código de la vista, y lo tenemos realizado.

Como van a ver en las siguientes imágenes, no necesariamente tiene que se una palabra, sino puede ser cualquier tipo de dato: entero, flotante, matrices, vectores.

**Imagen de como debe de quedar:**

![Ejemplo](Imagenes/Variables_a_la_vista.png)

![Ejemplo](Imagenes/Variables_a_la_vista_2.png)

------

**Agregar un item al menu**

Ahora que ya sabemos armar una URL hacia un controlador y una acción, y que sabemos agregar paginas nuevas a los controladores, vamos a agregar una sección a nuestro menú que nos dirija a algún controlador y acción.

Nos dirigimos a la carpeta de nuestro tema o temas que estemos usando, y abrimos el archivo **config.json**

Nos dirigimos a la sección **menu**
y agregamos una nueva sección con la siguiente estructura:

```json
    "identificador": {
        "title": "NombreAMostrar",
        "href": "DirecciónAlSitio",
        "show": "normal/disabled"
    }
```

En la parte de show deben elegir si entre normal (muesta el link tal cual) o disabled (muestra el link deshabilitado).

**Imagen de como debe quedar:**

![Ejemplo](Imagenes/menu.png)

![Ejemplo](Imagenes/menu2.png)

**A personalizar nuestro controlador!**
===================

**Cambiar de header, footer o vista para cierta acción del controlador**

Para realizar el cambio de header, footer o vista tenemos las siguientes funciones disponibles:

```php
View::setHeader('nombreDelArchivo');
```

```php
View::setView('nombreDelArchivo');
```

```php
View::setFooter('nombreDelArchivo');
```

Los archivos se ubican en la carpeta con los nombres headers, footers, y views(*Aquí se divide entre los controladores, por lo que debes estar atento a que controlador le estas poniendo una vista personalizada a su acción*) respectivamente, dentro de la carpeta del tema que estas usando.

**Atención: ** por el momento la función *View::setView();* no funciona en la versión 0.1.1a ni en la anterior 0.1a, esto será corregido en la próxima versión.

**Imagen de como debe quedar:**

![Ejemplo](Imagenes/header_footer_view.png)

![Ejemplo](Imagenes/header_footer_view_2.png)

------

**Redirecciones**

Para realizar redirecciones, basta con usar la función que tiene incluido nuestro controlador.

```php
$this->redirect('controlador', 'acción');
```

```php
$this->redirect('controlador', 'acción+parametros');
```

Ejemplo:

```php
$this->redirect('index', 'about');
```

```php
$this->redirect('math', 'suma/20/30/10');
```

------

**Quitando el limite de parámetros de las acciones**

*En Construción*

------

**Hacer multilenguaje la vista de nuestro controlador (Llenar nuestro archivo lang)**

Este archivo lo podemos encontrar en la carpeta core/lang/Idioma/Controlador/lang

Nuestro archivo de lang se encuentra formado así:

```json
{
    "controlador": {
        "accion": {
            "texto": "traducci&oacute;n"
        }
    }
}
```

En mi caso haré el archivo lang de mi controlador prueba

Abro el archivo: *core/lang/es/prueba/lang* y copio la estructura que esta arriba:

```json
{
    "prueba": {
        "index": {
            "greeting": "&#161;Hola&#33;"
        }
    }
}
```

Con esto tenemos disponible la "palabra" greeting, y solo nos faltaría agregar la traducción para los otros idiomas que tengamos en el sitio, respetando la misma estructura, solo cambiando la traducción.

```json
{
    "prueba": {
        "index": {
            "greeting": "Hi&#33;"
        }
    }
}
```

Ya solo nos queda irnos a nuestra vista de la acción a la cual le acabamos añadir una traducción y posicionar donde queremos que nos aparezca el texto y utilizar:

```html
{{ lang.texto }}
```

En mi caso sería
```html
{{ lang.greeting }}
```

**Imagen de como debe quedar:**

![Ejemplo](Imagenes/idioma.png)

Español:

![Ejemplo](Imagenes/idioma2.png)

Ingles:

![Ejemplo](Imagenes/idioma3.png)

**¡Manos a la obra!**
===================

**Interacción de nuestro controlador a la base de datos**

*En Construción*

------

**Creación de nuestro modelo para uso exclusivo del controlador**

*En Construción*

------

**Creación de nuestro modelo para uso común para todos los controladores.**

*En Construción*

------

**Obtención de datos**

*En Construción*

------

**Hacer que se ejecute un código "antes o después" de cargar el controlador**

*En Construción*

**Desarrollo de nuestro primer paquete**
===================

**Consejos**

*En Construción*

------

**¿Que funciones hay disponibles para usar?**

*En Construción*

------

**Estructura que debe tener nuestro paquete**

*En Construción*

------

**Como estructurar el archivo *info***

*En Construción*

------

**Como estructurar el archivo *install.in***

*En Construción*

------

**Como compartir nuestro paquete**

*En Construción*

------

**Como instalar un paquete**
===================

1. Descargamos el paquete deseado.
2. Descomprimimos el archivo en la carpeta **packages**, tratando de dejar los archivos en una subcarpeta ej: *El paquete de Sistema de usuarios, el archivo comprimido ya cuenta con una subcarpeta llamada **User**, esta debe colocarse en la carpeta **packages** tal cual esta.*
3. Nos dirigimos a nuestro navegador y luego a la siguiente url: [http://direcciónDeNuestroSitio/install/index/NombreDeLaCarpeta](http://direcciónDeNuestroSitio/install/index/NombreDeLaCarpeta), en mi caso quedaría => [http://localhost/sm/install/index/User](http://localhost/sm/install/index/User)
4. Si todo esta correcto, deberíamos ver una pagina como la que hay en la 2° imagen de arriba.
5. Una vez verificado todo lo que realizará el instalador, procedemos a darle "Instalar"
6. Nos arrojara una pagina con los cambios que se realizaron correctamente y cuales no, si todos los cambios fueron correctos, basta con ir al inicio de nuestro sitio, y ver los cambios.

------

**Paquetes disponibles**
===================

**Nombre:** Update for BXFork
**Versión:** 0.1.1a
**Descripción:**

- Corrección en listar los temas.
- Corrección en la carga de modelos.
- Corrección en la impresión de variables.
- Eliminación de secciones extras sin utilidad.
- Cambios menores.

**Compatibilidad BXFork:** 0.1a

--------

**Nombre:** Sistema de usuarios
**Versión:** 0.1.1
**Descripción:** Un sistema de usuarios basico, con un registro y un login; esta formado por un usuario y una contraseña. Cuenta con mensaje de contraseña incorrecta, usuario incorrecto y si hay un usuario con el mismo nombre a la hora de registrarse.
**Compatibilidad BXFork:** 0.1a

***Anteriores:***
**Versión:** 0.1
**Descripción:** Un sistema de usuarios basico, con un registro y un login; esta formado por un usuario y una contraseña. No cuenta con algún tipo de mensaje de error por registro incorrecto o login incorrecto.
**Compatibilidad BXFork:** 0.1a

Información

--------

- **Licencia del proyecto:** General Public License (**GPLv3**)
- **Fecha de lanzamiento:** 3 de Agosto del 2016
- **Ultima versión estable:** ~~~
- **Fecha de ultima versión:** 8 de Agosto del 2016
- **Versión:** 0.1.1a


--------

 BXFork | Framework básico

 Framework básico para un proyecto web

 Author: @BlackBlex (BlackBlex)

 License: General Public License (GPLv3) | http://www.gnu.org/licenses/
